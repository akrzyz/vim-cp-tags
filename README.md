### What is it ###
Set tags files generated with [tagsGen](https://bitbucket.org/akrzyz/scripts).
When vim is opened tags folder is set to *$projectroot/.tags*

When c++ file is opened c++ tags for given components are attached, same with ttcn files.

### Set up ###
Add to your vimrc: *let g:CPlaneTagsComponents  = ['component1', 'component2']*

### Configuration ###
*g:CPlaneTagsComponents* define components for which tags will be attached. Default value is *[ ]*

*g:CPlaneTagsLocation* allow to override folder with tags. Default value is set automaticity to *$projectroot/.tags*

*g:CPlaneTagsProjectRootMarkers* allow to define project root marker used for tags location discovery. Default value is *['.git','.config']*

*g:CPlaneTagsCppDefault* define tags file always added for c++ file. Default value is *['std_cpp', 'common_cpp', 'isar_cpp', 'intheaders_cpp']*

*g:CPlaneTagsTtcnDefault* define tags file always added for tccn file. Default value is *['common_ttcn3', 'k3lteasn_ttcn3']*

### Dependencies ###
if *g:CPlaneTagsLocation* is not set [vim-projectroot](https://github.com/dbakker/vim-projectroot) is required to find *$projectroot*
