" CplaneTags files plugin compatible with tags generated witg tagsGen
"
if !exists('g:CPlaneTagsProjectRootMarkers') | let g:CPlaneTagsProjectRootMarkers = ['.git','.config'] | endif
function s:CplanetTagsGetLocation()
    if exists('g:rootmarkers') | let s:rootMarkersBackup = g:rootmarkers | endif
    let g:rootmarkers = g:CPlaneTagsProjectRootMarkers
    let l:tagsLocations = projectroot#get() . "/.tags/"
    if exists('s:rootMarkersBackup') | let g:rootMarkers = s:rootMarkersBackup | endif
    return l:tagsLocations
endfunc

if !exists('g:CPlaneTagsLocation')    | let g:CPlaneTagsLocation    = s:CplanetTagsGetLocation() | endif
if !exists('g:CPlaneTagsComponents')  | let g:CPlaneTagsComponents  = [] | endif
if !exists('g:CPlaneTagsCppDefault')  | let g:CPlaneTagsCppDefault  = ['std_cpp', 'common_cpp', 'isar_cpp', 'intheaders_cpp'] | endif
if !exists('g:CPlaneTagsTtcnDefault') | let g:CPlaneTagsTtcnDefault = ['common_ttcn3', 'k3lteasn_ttcn3'] | endif

function! g:CplaneTagsInit(components)
    let s:CplaneTagsCpp =  map(copy(g:CPlaneTagsCppDefault), 'g:CPlaneTagsLocation . v:val')
    let s:CplaneTagsCpp += map(copy(a:components), 'g:CPlaneTagsLocation . v:val . "_cpp"')
    let s:CplaneTagsTtcn =  map(copy(g:CPlaneTagsTtcnDefault), 'g:CPlaneTagsLocation . v:val')
    let s:CplaneTagsTtcn += map(copy(a:components), 'g:CPlaneTagsLocation . v:val . "_ttcn3"')
endfunc

" add file with tags if not already added
function! s:CPlaneTagsAddFile(tagFile)
    if stridx(&tags, a:tagFile) == -1
        execute "set tags+=" . a:tagFile
    endif
endfunc

function! s:CplaneTagsCppInit()
    for tagFile in s:CplaneTagsCpp
        call s:CPlaneTagsAddFile(tagFile)
    endfor
endfunc

function! s:CplaneTagsTtcnInit()
    for tagFile in s:CplaneTagsTtcn
        call s:CPlaneTagsAddFile(tagFile)
    endfor
endfunc

call g:CplaneTagsInit(g:CPlaneTagsComponents)
autocmd BufRead,BufNewFile *.c,*.cpp,*.h,*.hpp,*.out :call s:CplaneTagsCppInit()
autocmd BufRead,BufNewFile *.ttcn,*.ttcn3,*.txt,*.log :call s:CplaneTagsTtcnInit()

